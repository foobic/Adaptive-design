var gulp = require('gulp'), 
    sass = require('gulp-sass'), // convert SASS to  CSS
    browserSync = require('browser-sync'), // synchronization with browser for quick testing without reload
    concat      = require('gulp-concat'), // for files concatenation
    uglify      = require('gulp-uglifyjs'), // for JS compression
    cssnano     = require('gulp-cssnano'), // for CSS minification
    rename      = require('gulp-rename'), // plugin to rename files
    del         = require('del'); // library for deleting files and folders


gulp.task('sass', function(){
    return gulp.src('src/sass/**/*.+(scss|sass)')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('src/css/'))
        .pipe(browserSync.reload({stream: true}))
})

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'src'
        },
        open: false, // if npm doesn't have permission for opening browser
        notify: false
    });
});


gulp.task('watch',['browser-sync'], function() {
    gulp.watch('src/sass/**/*.+(scss|sass)', ['sass']);
    gulp.watch('src/**/*.html', browserSync.reload); 
    // gulp.watch('src/js#<{(||)}>#*.js', browserSync.reload);  // BrowserSync for JavaScript, later.
});


// Production
gulp.task('clean', function() {
    return del.sync('dist'); 

});


gulp.task('build', ['clean', 'sass'], function() {

    var buildCss = gulp.src([ 
        'src/css/main.css',
    ])
        .pipe(gulp.dest('dist/css'))

    var buildFonts = gulp.src('src/fonts/**/*') 
        .pipe(gulp.dest('dist/fonts'))

    var buildHtml = gulp.src('src/*.html')
        .pipe(gulp.dest('dist'));

});

