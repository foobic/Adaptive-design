let gulp = require('gulp'), 
    sass = require('gulp-sass'), // convert SASS to  CSS
    browserSync = require('browser-sync'), // synchronization with browser for quick testing without reload
    concat      = require('gulp-concat'), // for files concatenation
    uglify      = require('gulp-uglifyjs'), // for JS compression
    cssnano     = require('gulp-cssnano'), // for CSS minification
    rename      = require('gulp-rename'), // plugin to rename files
    del         = require('del'), // library for deleting files and folders
    autoprefixer = require('gulp-autoprefixer'), // auto adding css prefixes
    htmlmin = require('gulp-htmlmin'),
    plumber = require('gulp-plumber');


let paths = {
    src: './src', 
    dist: './dist',
    // css: {
    //     src: '/css'
    // },
    css: '/css',
    sass: {
        src: '/sass',
        dest: '/css'
    },
    html: '/**/*.html',
    js: '/js/**/*.js',
    libs: {
        css: '/css/libs',
        js: '/js/libs'
    }
};

gulp.task('sass', function(){
    return gulp.src(paths.src + paths.sass.src + '/**/*.+(scss|sass)')
        .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
        // .pipe(sass({
        //     onError: browserSync.notify
        //     onError: browserSync.notify(messages.sassError)
        // }))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true  }))
        .pipe(gulp.dest(paths.src + paths.sass.dest))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: paths.src
        },
        open: false, // if npm doesn't have permission for opening browser
        notify: false
    });
});


gulp.task('watch',['browser-sync'], function() {
    gulp.watch(paths.src + paths.sass.src + '/**/*.+(scss|sass)', ['sass']);
    gulp.watch(paths.src + paths.html, browserSync.reload); 
    gulp.watch(paths.src + paths.js, browserSync.reload);  
});






// Production
gulp.task('clean', function() {
    return del.sync('dist'); 

});

gulp.task('libs-css-minify',['sass'],function(){
    return gulp.src(paths.src + paths.libs.css + '/**/*.css')
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.dist + paths.libs.css))
});

gulp.task('css-minify',['libs-css-minify'],function(){
    // console.log(paths.dist + paths.css);
    // console.log(paths.src + paths.css,'!' + paths.src + paths.libs.sass.src);
    return gulp.src([paths.src + paths.css + '/**/*.css', '!' + paths.src + paths.libs.css ,'!' + paths.src + paths.libs.css + '/**/*.css'])
        .pipe(cssnano())
        .pipe(gulp.dest(paths.dist + paths.css))
});

gulp.task('html-minify', function(){
    return gulp.src(paths.src + '/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(paths.dist));
});



gulp.task('build', ['clean', 'html-minify', 'css-minify'], function() {

});

gulp.task('default', ['sass','watch']);

